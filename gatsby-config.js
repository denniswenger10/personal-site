const { resolve: resolvePath } = require('path')

const manifestOptions = {
  name: `Dennis Wenger - Personal Site`,
  short_name: `Dennis Wenger`,
  start_url: `/`,
  background_color: `#486C7B`,
  theme_color: `#486C7B`,
  display: `minimal-ui`,
  //icon: `content/assets/gatsby-icon.png`,
}

const fonts = [
  {
    family: `Baloo Chettan 2`,
    variants: [`400`, `500`, `600`, `700`, `800`],
  },
  {
    family: `Londrina Solid`,
    variants: [`100`, `300`, `400`, `900`],
  },
  {
    family: `Fira Code`,
    variants: [`300`, `400`, `500`, `600`, `700`],
  },
]

const pathAliases = {
  '@src': resolvePath(__dirname, 'src'),
  '@components': resolvePath(__dirname, 'src/components'),
  '@pure-components': resolvePath(__dirname, 'src/pure-components'),
  '@pages': resolvePath(__dirname, 'src/pages'),
  '@common': resolvePath(__dirname, 'src/common'),
  '@styles': resolvePath(__dirname, 'src/styles'),
  '@hooks': resolvePath(__dirname, 'src/hooks'),
  '@common': resolvePath(__dirname, 'src/common'),
  '@content': resolvePath(__dirname, 'content'),
  '@context': resolvePath(__dirname, 'src/state/context'),
}

const fileSystem = [
  {
    path: `${__dirname}/content/data`,
    name: `data`,
  },
  {
    path: `${__dirname}/content/text-fields`,
    name: `text-fields`,
  },
  {
    path: `${__dirname}/content/thoughts`,
    name: `thoughts`,
  },
].map(({ path, name }) => ({
  resolve: `gatsby-source-filesystem`,
  options: {
    path,
    name,
  },
}))

module.exports = {
  plugins: [
    {
      resolve: `gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts,
      },
    },
    `gatsby-plugin-emotion`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-json`,
    `gatsby-plugin-typescript`,
    `gatsby-plugin-mdx`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 590,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-prismjs`, // code blocks
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`, // punctuation - > becomes -> and more
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: manifestOptions,
    },
    ...fileSystem,
    {
      resolve: `gatsby-plugin-alias-imports`,
      options: {
        alias: pathAliases,
        extensions: [],
      },
    },
  ],
}
