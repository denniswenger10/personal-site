import React, { useState, ReactNode } from 'react'
import {
  colorThemeStandard,
  colorThemeDarkRed,
  colorThemeLight,
} from '@styles/color-theme/themes'
import { ColorThemeContext } from './color-theme-context'
import { LoadColorTheme } from '@components/load-color-theme'

type ColorThemeProviderProps = {
  children: ReactNode
}

const ColorThemeProvider = ({
  children = [],
}: ColorThemeProviderProps) => {
  const [colorTheme, setColorTheme] = useState(colorThemeStandard)

  console.log('Provider is reloaded')

  return (
    <ColorThemeContext.Provider value={{ colorTheme, setColorTheme }}>
      <LoadColorTheme colorTheme={colorTheme} children={children} />
    </ColorThemeContext.Provider>
  )
}

export { ColorThemeProvider }
