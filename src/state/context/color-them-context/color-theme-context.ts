import { createContext } from 'react'
import { ColorTheme } from '@styles/color-theme'
import { colorThemeStandard } from '@styles/color-theme/themes'

type ColorThemeContextType = {
  colorTheme: ColorTheme
  setColorTheme: (value: ColorTheme) => void
}

const ColorThemeContext = createContext<ColorThemeContextType>({
  colorTheme: colorThemeStandard,
  setColorTheme: () => {},
})

export { ColorThemeContext }
