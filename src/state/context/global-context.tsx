import React from 'react'
import { ColorThemeProvider } from './color-them-context'

export default ({ element }) => (
  <ColorThemeProvider>{element}</ColorThemeProvider>
)
