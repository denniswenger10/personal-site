import { css } from '@emotion/core'
import { TEXT_STYLE } from '@styles'
import {
  ColorThemeKey,
  hslaFromColorThemeKey,
  hslFromColorThemeKey,
} from '@styles/color-theme'
import React from 'react'

type ChipProps = {
  text: string
}

const Chip = ({ text = '' }: ChipProps) => {
  const style = css([
    TEXT_STYLE.body[2],
    {
      padding: '2px 8px 3px 8px',
      background: hslaFromColorThemeKey(
        ColorThemeKey.SecondaryBackground
      ),
      color: hslFromColorThemeKey(ColorThemeKey.SecondaryForeground),
      borderRadius: '5px',
    },
  ])

  return <span css={style}>{text}</span>
}

export { Chip }
