import React from 'react'
import { css } from '@emotion/core'

type DividerProps = {
  width?: string
  height?: string
  color?: string
  cap?: 'round' | 'square'
}

const Divider = ({
  width = '100%',
  height = '1px',
  color = 'black',
  cap = 'round',
}: DividerProps) => {
  const style = css({
    width,
    height,
    background: color,
    borderRadius: cap === 'round' ? '9999px' : 0,
  })

  return <div css={style} />
}

export { Divider }
