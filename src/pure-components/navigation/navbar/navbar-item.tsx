import React, { ReactNode } from 'react'
import { css, SerializedStyles } from '@emotion/core'

type NavbarItemProps = {
  children?: ReactNode
  padding?: string
  fullWidth?: boolean
  style?: SerializedStyles | SerializedStyles[]
}

const NavbarItem = ({
  children = null,
  fullWidth = true,
  style = css(),
}: NavbarItemProps) => {
  const styles = css({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: fullWidth ? '100%' : '',
    position: 'relative',
    '&:focus': {
      outline: 'none',
    },
  })

  return <div css={[styles, style]}>{children}</div>
}

export { NavbarItem }
