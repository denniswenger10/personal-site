import React, { ReactNode } from 'react'
import { css, SerializedStyles } from '@emotion/core'

type NavbarProps = {
  children?: ReactNode
  background?: string
  style?: SerializedStyles | SerializedStyles[]
}

const Navbar = ({
  children = [],
  background = 'black',
  style = css(),
}: NavbarProps) => {
  const styles = css([
    {
      background,
      width: '100%',
      display: 'flex',
    },
    style,
  ])

  return <div css={styles}>{children}</div>
}

export { Navbar }
