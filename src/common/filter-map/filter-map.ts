/**
 *
 * @param filter Function to use as filter. Will be run on every item in the array.
 * @param list Array to filter and map
 *
 * Will run the function on every item in the array, like a map, and return a new array.
 * Difference being it will filter out items where the function returns `undefined`.
 * Remember that javascript returns `undefined` when nothing is returned.
 */
const filterMap = <T, K>(
  filter: (value: T, index?: number) => K | undefined | void,
  list: T[]
): K[] => {
  return list.reduce((acc: K[], curr, index) => {
    const item = filter(curr, index)

    if (item) {
      acc.push(item)
    }

    return acc
  }, [])
}

export { filterMap }
