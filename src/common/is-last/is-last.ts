const isLast = (list: any[], index: number) =>
  index >= list.length - 1

export { isLast }
