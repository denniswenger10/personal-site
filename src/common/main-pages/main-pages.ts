enum MainPage {
  Home = 'Home',
  Thoughts = 'Thoughts',
  About = 'About',
  Contact = 'Contact',
}

const urlMap = new Map([
  [MainPage.Home, '/'],
  [MainPage.Thoughts, '/thoughts'],
  [MainPage.About, '/about'],
  [MainPage.Contact, '/contact'],
])

const mainPageToText = (mainPage: MainPage): string => mainPage

const mainPageToUrl = (mainPage: MainPage): string =>
  urlMap.get(mainPage) || '/'

export { MainPage, mainPageToText, mainPageToUrl }
