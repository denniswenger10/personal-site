const countWords = (text = '') => {
  const removedLinks = text.replace(/\[([^[\]]*)\]\((.*?)\)/gm, '$1')

  const matches = removedLinks.match(/[\w\d’'-]+/gi)

  return matches ? matches.length : 0
}

module.exports = { countWords }
