const minutesToRead = (wpm = 200) => (amountOfWords = 0) => {
  return Math.ceil(amountOfWords / wpm)
}

module.exports = { minutesToRead }
