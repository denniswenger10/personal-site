import React from 'react'
import { Divider } from '@pure-components/graphics/divider'
import {
  ColorThemeKey,
  hslFromColorThemeKey,
} from '@styles/color-theme'

const DwDivider = () => {
  return (
    <Divider color={hslFromColorThemeKey(ColorThemeKey.Divider)} />
  )
}

export { DwDivider }
