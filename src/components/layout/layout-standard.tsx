import React, { ReactNode } from 'react'
import { GLOBAL } from '@styles/global'
import { css, Global } from '@emotion/core'
import { DwNavbar } from '@src/components/navigation/dw-navbar'

type StandardLayoutProps = {
  children: ReactNode
}

const StandardLayout = ({ children }: StandardLayoutProps) => {
  const globalStyle = GLOBAL

  const style = css({
    padding: '32px',
  })

  return (
    <>
      <Global styles={globalStyle} />
      <div css={style}>{children}</div>
      <DwNavbar />
    </>
  )
}

export { StandardLayout }
