import React, { ReactNode } from 'react'
import { GLOBAL } from '@styles/global'
import { css, Global } from '@emotion/core'
import { MDXProvider } from '@mdx-js/react'
import { DwNavbar } from '@src/components/navigation/dw-navbar'
import { components } from '@components/mdx'

type LayoutMdx = {
  children: ReactNode
}

const LayoutMdx = ({ children }: LayoutMdx) => {
  const globalStyle = GLOBAL

  const style = css({
    padding: '32px',
    width: '92vw',
    maxWidth: '800px',
    margin: 'auto',
  })

  return (
    <>
      <Global styles={globalStyle} />
      <div css={style}>
        <MDXProvider components={components}>{children}</MDXProvider>
      </div>
      <DwNavbar />
    </>
  )
}

export { LayoutMdx }
