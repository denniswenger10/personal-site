import React, { ReactNode } from 'react'
import { ColorTheme } from '@styles/color-theme'
import { Global, css } from '@emotion/core'

type LoadColorThemeProps = {
  children: ReactNode
  colorTheme: ColorTheme
}

const LoadColorTheme = ({
  children,
  colorTheme,
}: LoadColorThemeProps) => {
  const style = css({
    ':root': colorTheme.reduce((prev, [key, value]) => {
      return { ...prev, [key]: value }
    }, {}),
  })

  return (
    <>
      <Global styles={style} />
      {children}
    </>
  )
}

export { LoadColorTheme }
