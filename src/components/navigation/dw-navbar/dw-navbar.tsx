import React from 'react'
import { css } from '@emotion/core'
import { Navbar } from '@pure-components/navigation/navbar'
import {
  ColorThemeKey,
  hslaFromColorThemeKey,
  hslFromColorThemeKey,
} from '@styles/color-theme'
import { MainPage } from '@common'
import { DwNavbarItem } from './dw-navbar-item'
import { DwLink } from '../dw-link'

const DwNavbar = () => {
  const items = [MainPage.Thoughts, MainPage.About, MainPage.Contact]

  const style = css({
    position: 'fixed',
    width: '100%',
    top: 0,
    left: 0,
    background: hslaFromColorThemeKey(
      ColorThemeKey.SecondaryBackground,
      0.8
    ),
    overflow: 'hidden',
    backdropFilter: 'blur(5px)',
    padding: '0 12px',
  })

  const navbarStyle = css({
    justifyContent: 'flex-end',
    alignItems: 'center',
  })

  const homeStyle = css({
    height: '32px',
    width: '32px',
    background: hslFromColorThemeKey(ColorThemeKey.Highlight),
    marginRight: 'auto',
  })

  return (
    <div css={style}>
      <Navbar background={'transparent'} style={navbarStyle}>
        <DwLink to="/" style={homeStyle}></DwLink>
        {items.map((item) => (
          <DwNavbarItem key={item} mainPage={item} />
        ))}
      </Navbar>
    </div>
  )
}

export { DwNavbar }
