import React from 'react'
import { css } from '@emotion/core'
import { NavbarItem } from '@pure-components/navigation/navbar'
import {
  ColorThemeKey,
  hslaFromColorThemeKey,
} from '@styles/color-theme'
import { TEXT_STYLE } from '@styles/text-style'
import { MainPage, mainPageToUrl } from '@common'
import { DwLink } from '@components/navigation/dw-link'

type DwNavbarItemProps = {
  mainPage: MainPage
}

const DwNavbarItem = ({ mainPage }: DwNavbarItemProps) => {
  const itemStyle = css([
    TEXT_STYLE.body[1],
    {
      color: hslaFromColorThemeKey(ColorThemeKey.SecondaryForeground),
      padding: '20px 18px',
    },
  ])

  return (
    <NavbarItem fullWidth={false}>
      <DwLink to={mainPageToUrl(mainPage)} style={itemStyle}>
        {mainPage}
      </DwLink>
    </NavbarItem>
  )
}

export { DwNavbarItem }
