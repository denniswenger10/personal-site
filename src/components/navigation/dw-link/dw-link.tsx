import { Link } from 'gatsby'
import React, { ReactNode } from 'react'
import { css as _css, SerializedStyles } from '@emotion/core'

type DwLinkProps = {
  children?: ReactNode
  to: string
  style?: SerializedStyles | SerializedStyles[]
}

const DwLink = ({
  children = [],
  to,
  style = _css(),
}: DwLinkProps) => {
  const _style = _css([
    {
      boxShadow: 'none',
      textDecoration: 'none',
      color: 'inherit',
    },
    {
      ...style,
    },
  ])

  return (
    <Link css={_style} to={to}>
      {children}
    </Link>
  )
}

export { DwLink }
