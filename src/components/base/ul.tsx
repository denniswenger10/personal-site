import styled from '@emotion/styled'
import { TEXT_STYLE } from '@src/styles'
import {
  ColorThemeKey,
  hslFromColorThemeKey,
} from '@src/styles/color-theme'

const Ul = styled.ul([
  TEXT_STYLE.body[1],
  {
    color: hslFromColorThemeKey(ColorThemeKey.PrimaryForeground),
    marginLeft: '18px',
    '&>li': {
      p: {
        display: 'inline',
      },
      '&::before': {
        content: '"•"',
        color: hslFromColorThemeKey(ColorThemeKey.Accent),
        fontSize: '32px',
        verticalAlign: '-4px',
        marginRight: '8px',
      },
      '&>ul>li::before': {
        content: '"○"',
        fontSize: '16px',
        verticalAlign: '0px',
      },
      '&>ul>li>ul>li::before': {
        content: '"■"',
        fontSize: '10px',
        verticalAlign: '2px',
      },
    },
  },
])

export { Ul }
