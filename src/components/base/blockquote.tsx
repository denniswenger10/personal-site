import styled from '@emotion/styled'
import { TEXT_STYLE } from '@src/styles'
import {
  ColorThemeKey,
  hslaFromColorThemeKey,
  hslFromColorThemeKey,
} from '@src/styles/color-theme'

const backgroundColor = hslaFromColorThemeKey(
  ColorThemeKey.ForegroundContrastLow,
  0.15
)
const textColor = hslFromColorThemeKey(
  ColorThemeKey.PrimaryForeground
)
const leftBorderColor = hslFromColorThemeKey(
  ColorThemeKey.PrimaryForeground
)
const quoteMarkColor = hslFromColorThemeKey(ColorThemeKey.Accent)

const Blockquote = styled.blockquote([
  TEXT_STYLE.body[1],
  {
    background: backgroundColor,
    color: textColor,
    position: 'relative',
    borderLeft: `2px solid ${leftBorderColor}`,
    padding: '12px 8px 8px 12px',
    marginTop: '12px',
    borderRadius: '5px',
    quotes: '"“" "”" "“" "’"',
    '&::before': {
      color: quoteMarkColor,
      fontWeight: 500,
      content: 'open-quote',
      fontSize: '40px',
      lineHeight: '24px',
      marginRight: '8px',
      verticalAlign: '-8px',
    },
    p: {
      display: 'inline',
    },
  },
])

export { Blockquote }
