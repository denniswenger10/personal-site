import styled from '@emotion/styled'

const Em = styled.em({
  fontStyle: 'italic',
})

export { Em }
