import styled from '@emotion/styled'
import { TEXT_STYLE } from '@src/styles'
import {
  ColorThemeKey,
  hslaFromColorThemeKey,
  hslFromColorThemeKey,
} from '@src/styles/color-theme'

const Table = styled.table([
  TEXT_STYLE.body[1],
  {
    color: hslFromColorThemeKey(ColorThemeKey.PrimaryForeground),
    marginTop: '12px',
    marginLeft: '18px',
    textAlign: 'left',
    padding: '8px',
    th: {
      fontWeight: 500,
      padding: '4px 12px',
      borderBottom: `2px solid ${hslFromColorThemeKey(
        ColorThemeKey.Divider
      )}`,
    },
    td: {
      padding: '4px 12px',
    },
    'tr:nth-of-type(even)': {
      background: hslaFromColorThemeKey(ColorThemeKey.Divider, 0.1),
    },
  },
])

export { Table }
