import styled from '@emotion/styled'
import {
  ColorThemeKey,
  hslFromColorThemeKey,
} from '@src/styles/color-theme'

const A = styled.a({
  color: hslFromColorThemeKey(ColorThemeKey.Highlight),
  transition: 'color 250ms ease',
  '&:hover': {
    color: hslFromColorThemeKey(ColorThemeKey.HighlightHover),
  },
})

export { A }
