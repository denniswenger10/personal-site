import React, { ReactNode } from 'react'
import { Light as SyntaxHighlighter } from 'react-syntax-highlighter'
// import anOldHope from 'react-syntax-highlighter/dist/esm/styles/hljs/srcery'
// import anOldHope from 'react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark-reasonable'
// import anOldHope from 'react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark'
// import anOldHope from 'react-syntax-highlighter/dist/esm/styles/hljs/ocean'
// import anOldHope from 'react-syntax-highlighter/dist/esm/styles/hljs/atelier-savanna-dark'
import anOldHope from 'react-syntax-highlighter/dist/esm/styles/hljs/hybrid'
import { TEXT_STYLE } from '@styles'
import { programmingLanguages } from './programming-languages'
import { css } from '@emotion/core'
import {
  ColorThemeKey,
  hslaFromColorThemeKey,
} from '@src/styles/color-theme'

programmingLanguages.forEach(([name, lib]) =>
  SyntaxHighlighter.registerLanguage(name, lib)
)

type CodeProps = {
  children: ReactNode
}

const Code = ({ children = [] }: CodeProps) => {
  const style = css([TEXT_STYLE.code[2]])

  return (
    <SyntaxHighlighter
      language="javascript"
      style={anOldHope}
      css={style}
      customStyle={{
        background: hslaFromColorThemeKey(
          ColorThemeKey.AlwaysBlack,
          0.9
        ),
        padding: '24px 24px 12px 24px',
        marginTop: '12px',
        // marginLeft: '18px',
        borderRadius: '5px',
      }}
    >
      {children}
    </SyntaxHighlighter>
  )
}

export { Code }
