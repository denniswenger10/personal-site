import js from 'react-syntax-highlighter/dist/esm/languages/hljs/javascript'
import ts from 'react-syntax-highlighter/dist/esm/languages/hljs/typescript'
import rust from 'react-syntax-highlighter/dist/esm/languages/hljs/rust'
import powershell from 'react-syntax-highlighter/dist/esm/languages/hljs/powershell'
import dart from 'react-syntax-highlighter/dist/esm/languages/hljs/dart'
import json from 'react-syntax-highlighter/dist/esm/languages/hljs/json'
import markdown from 'react-syntax-highlighter/dist/esm/languages/hljs/markdown'
import xml from 'react-syntax-highlighter/dist/esm/languages/hljs/xml'
import bash from 'react-syntax-highlighter/dist/esm/languages/hljs/bash'
import css from 'react-syntax-highlighter/dist/esm/languages/hljs/css'
import dockerfile from 'react-syntax-highlighter/dist/esm/languages/hljs/dockerfile'
import go from 'react-syntax-highlighter/dist/esm/languages/hljs/go'
import ini from 'react-syntax-highlighter/dist/esm/languages/hljs/ini'
import plain from 'react-syntax-highlighter/dist/esm/languages/hljs/plaintext'
import python from 'react-syntax-highlighter/dist/esm/languages/hljs/python'
import scss from 'react-syntax-highlighter/dist/esm/languages/hljs/scss'
import shell from 'react-syntax-highlighter/dist/esm/languages/hljs/shell'
import yaml from 'react-syntax-highlighter/dist/esm/languages/hljs/yaml'

const programmingLanguages = [
  ['javascript', js],
  ['typescript', ts],
  ['rust', rust],
  ['powershell', powershell],
  ['dart', dart],
  ['json', json],
  ['markdown', markdown],
  ['xml', xml],
  ['html', xml],
  ['bash', bash],
  ['css', css],
  ['dockerfile', dockerfile],
  ['go', go],
  ['toml', ini],
  ['ini', ini],
  ['plaintext', plain],
  ['', plain],
  ['text', plain],
  ['txt', plain],
  ['python', python],
  ['scss', scss],
  ['shell', shell],
  ['yaml', yaml],
]

export { programmingLanguages }
