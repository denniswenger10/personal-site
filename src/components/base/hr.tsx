import styled from '@emotion/styled'
import {
  ColorThemeKey,
  hslFromColorThemeKey,
} from '@src/styles/color-theme'

const Hr = styled.hr({
  border: 'none',
  borderTop: `2px solid ${hslFromColorThemeKey(
    ColorThemeKey.Divider
  )}`,
  marginBlockStart: '12px',
  marginBlockEnd: '12px',
})

export { Hr }
