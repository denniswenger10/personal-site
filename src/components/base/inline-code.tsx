import styled from '@emotion/styled'
import { TEXT_STYLE } from '@src/styles'
import {
  ColorThemeKey,
  hslaFromColorThemeKey,
  hslFromColorThemeKey,
} from '@src/styles/color-theme'

const InlineCode = styled.code([
  TEXT_STYLE.code[2],
  {
    color: hslFromColorThemeKey(ColorThemeKey.Accent),
    background: hslaFromColorThemeKey(ColorThemeKey.AlwaysBlack, 0.8),
    padding: '1px 6px 3px 6px',
    borderRadius: '4px',
    marginLeft: '1px',
    marginRight: '1px',
  },
])

export { InlineCode }
