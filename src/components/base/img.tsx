import styled from '@emotion/styled'

const Img = styled.img({
  maxWidth: '100%',
  margin: 'auto',
  display: 'block',
})

export { Img }
