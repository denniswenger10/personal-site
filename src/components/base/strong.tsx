import styled from '@emotion/styled'

const Strong = styled.strong({
  fontWeight: 600,
})

export { Strong }
