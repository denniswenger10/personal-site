import styled from '@emotion/styled'
import { TEXT_STYLE } from '@src/styles'
import {
  ColorThemeKey,
  hslFromColorThemeKey,
} from '@src/styles/color-theme'

const Ol = styled.ol([
  TEXT_STYLE.body[1],
  {
    color: hslFromColorThemeKey(ColorThemeKey.PrimaryForeground),
    marginLeft: '18px',
    counterReset: 'li',
    li: {
      counterIncrement: 'li',
      p: {
        display: 'inline',
      },
      '&::before': {
        content: 'counter(li)"."',
        color: hslFromColorThemeKey(ColorThemeKey.Accent),
        // fontSize: '18px',
        marginRight: '8px',
      },
    },
  },
])

export { Ol }
