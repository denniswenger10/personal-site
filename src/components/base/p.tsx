import styled from '@emotion/styled'
import {
  ColorThemeKey,
  hslFromColorThemeKey,
} from '@src/styles/color-theme'
import { TEXT_STYLE } from '@styles'

const P = styled.p([
  TEXT_STYLE.body[1],
  {
    marginTop: '12px',
    color: hslFromColorThemeKey(ColorThemeKey.PrimaryForeground),
  },
])

export { P }
