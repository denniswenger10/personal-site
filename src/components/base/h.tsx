import { css } from '@emotion/core'
import styled from '@emotion/styled'
import { TEXT_STYLE } from '@src/styles'
import {
  ColorThemeKey,
  hslFromColorThemeKey,
} from '@src/styles/color-theme'

const baseStyling = css({
  marginTop: '18px',
  color: hslFromColorThemeKey(ColorThemeKey.PrimaryForeground),
})

const H1 = styled.h1([TEXT_STYLE.heading[3], baseStyling])
const H2 = styled.h2([TEXT_STYLE.heading[4], baseStyling])
const H3 = styled.h3([TEXT_STYLE.heading[5], baseStyling])
const H4 = styled.h4([TEXT_STYLE.heading[5], baseStyling])
const H5 = styled.h5([TEXT_STYLE.heading[5], baseStyling])
const H6 = styled.h6([TEXT_STYLE.heading[5], baseStyling])

export { H1, H2, H3, H4, H5, H6 }
