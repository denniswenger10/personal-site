import React from 'react'
import { css } from '@emotion/core'
import {
  hslFromColorThemeKey,
  ColorThemeKey,
} from '@styles/color-theme'
import { TEXT_STYLE } from '@styles/text-style'

const Landing = () => {
  const overHeaderText = 'Hello, my name is Dennis Wenger'
  const headerText = 'I am a software developer.'
  const underHeaderText =
    'Currently working as a project manager and developer at Conqr in Norrköping, Sweden'

  const overHeaderStyle = css([
    TEXT_STYLE.preHeading[2],
    {
      color: hslFromColorThemeKey(ColorThemeKey.Accent),
    },
  ])

  const headerStyle = css([
    TEXT_STYLE.heading[2],
    {
      marginTop: '8px',
      color: hslFromColorThemeKey(ColorThemeKey.PrimaryForeground),
    },
  ])

  const underHeaderStyle = css([
    TEXT_STYLE.body[1],
    {
      marginTop: '8px',
      color: hslFromColorThemeKey(ColorThemeKey.PrimaryForeground),
    },
  ])

  return (
    <div>
      <p css={overHeaderStyle}>{overHeaderText}</p>
      <h1 css={headerStyle}>{headerText}</h1>
      <p css={underHeaderStyle}>{underHeaderText}</p>
    </div>
  )
}

export { Landing }
