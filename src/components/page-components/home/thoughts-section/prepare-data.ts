type ThoughtNode = {
  fields: {
    minutesToRead: number
    slug: string
  }
  frontmatter: {
    title: string
    creationDate: string
    tags: string[]
  }
}

export type Query = {
  allMdx: {
    nodes: ThoughtNode[]
  }
}

export type Thought = {
  minutesToRead: number
  slug: string
  title: string
  creationDate: string
  tags: string[]
}

export const prepareData = (query: Query): Thought[] => {
  return (
    query?.allMdx?.nodes.map(
      ({ fields, frontmatter }): Thought => ({
        minutesToRead: fields.minutesToRead,
        slug: fields.slug,
        title: frontmatter.title,
        creationDate: frontmatter.creationDate,
        tags: frontmatter.tags,
      })
    ) ?? []
  )
}
