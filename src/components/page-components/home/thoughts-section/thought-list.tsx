import React, { Fragment } from 'react'
import { css, SerializedStyles } from '@emotion/core'
import { roundedBorder } from '@styles/rounded-border'
import { Thought } from './prepare-data'
import { ThoughtItem } from './thought-item'
import { DwDivider } from '@components/dw-divider'
import { isLast } from '@common'

type ThoughtListProps = {
  thoughts: Thought[]
  styles?: SerializedStyles | SerializedStyles[]
}

const ThoughtList = ({
  thoughts = [],
  styles = [],
}: ThoughtListProps) => {
  const thoughtListStyle = css([roundedBorder, styles])

  const buildThoughtItem = (thought: Thought, index) => (
    <Fragment key={thought.slug}>
      <ThoughtItem thought={thought} />
      {!isLast(thoughts, index) ? <DwDivider /> : null}
    </Fragment>
  )

  return (
    <div css={thoughtListStyle}>
      {thoughts.map((thought, index) =>
        buildThoughtItem(thought, index)
      )}
    </div>
  )
}

export { ThoughtList }
