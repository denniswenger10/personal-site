import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { Query, prepareData } from './prepare-data'
import { ThoughtList } from './thought-list'
import { css } from '@emotion/core'
import {
  ColorThemeKey,
  hslFromColorThemeKey,
} from '@src/styles/color-theme'
import { TEXT_STYLE } from '@src/styles'
import { DwLink } from '@src/components/navigation/dw-link'

const query = graphql`
  query {
    allMdx(
      sort: { fields: frontmatter___creationDate, order: DESC }
      limit: 5
    ) {
      nodes {
        fields {
          minutesToRead
          slug
        }
        frontmatter {
          title
          creationDate(formatString: "D MMM, YYYY")
          tags
        }
      }
    }
  }
`

const ThoughtsSection = () => {
  const data: Query = useStaticQuery(query)
  const thoughts = prepareData(data)

  const thoughtListStyle = css({
    marginTop: '12px',
    marginBottom: '12px',
  })

  const styles = css({
    display: 'grid',
    '.text': [
      TEXT_STYLE.button[1],
      {
        color: hslFromColorThemeKey(
          ColorThemeKey.ForegroundContrastHigh
        ),
      },
    ],
    '.right': {
      justifySelf: 'right',
    },
  })

  return (
    <div css={styles}>
      <h6 className="text">Recent thoughts</h6>
      <ThoughtList thoughts={thoughts} styles={thoughtListStyle} />

      <span className="text right">
        <DwLink to="/thoughts">More thoughts.. {'>>'}</DwLink>
      </span>
    </div>
  )
}

export { ThoughtsSection }
