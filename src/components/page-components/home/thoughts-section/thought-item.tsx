import React from 'react'
import { css } from '@emotion/core'
import { Thought } from './prepare-data'
import {
  ColorThemeKey,
  hslaFromColorThemeKey,
  hslFromColorThemeKey,
} from '@styles/color-theme'
import { TEXT_STYLE } from '@styles/text-style'
import { DwLink } from '@components/navigation/dw-link'
import { focusLight } from '@styles/focus'
import { Chip } from '@pure-components/data-display'

const ThoughtItem = ({ thought }: { thought: Thought }) => {
  const { creationDate, minutesToRead, slug, title, tags } = thought

  console.log(tags)

  const style = css([
    focusLight,
    {
      color: hslFromColorThemeKey(ColorThemeKey.PrimaryForeground),
      padding: '24px',
    },
  ])

  const headerStyle = css(TEXT_STYLE.body[1])

  const subHeaderStyle = css([
    TEXT_STYLE.body[2],
    {
      color: hslFromColorThemeKey(
        ColorThemeKey.ForegroundContrastHigh
      ),
    },
  ])

  const chipSectionStyle = css({
    marginTop: '12px',
    display: 'flex',
    gap: '8px',
    // justifyContent: 'flex-end',
  })

  return (
    <DwLink to={slug} css={style}>
      <div css={style}>
        <h3 css={headerStyle}>{title}</h3>
        <div>
          <p
            css={subHeaderStyle}
          >{`${creationDate} - ${minutesToRead} min`}</p>
        </div>
        <div css={chipSectionStyle}>
          {tags.map((tag) => (
            <Chip text={tag} />
          ))}
        </div>
      </div>
    </DwLink>
  )
}

export { ThoughtItem }
