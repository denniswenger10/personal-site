import { css } from '@emotion/core'
import React, { ReactNode } from 'react'

type AdjacentThoughtSectionProps = {
  children: ReactNode
}

const AdjacentThoughtSection = ({
  children = [],
}: AdjacentThoughtSectionProps) => {
  const style = css({
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    alignContent: 'center',
    justifyContent: 'space-between',
    gap: '12px',
    marginTop: '24px',
  })

  return <div css={style}>{children}</div>
}

export { AdjacentThoughtSection }
