import {
  ColorThemeKey,
  hslFromColorThemeKey,
} from '@styles/color-theme'
import { TEXT_STYLE } from '@styles'
import { roundedBorder } from '@styles/rounded-border'
import React from 'react'
import { DwLink } from '@components/navigation/dw-link'
import { css } from '@emotion/core'

enum ThoughtOrder {
  Next = 'next',
  Previous = 'previous',
}

type AdjacentThoughtProps = {
  slug: string
  title: string
  thoughtOrder: ThoughtOrder
}

const AdjacentThought = ({
  slug,
  title,
  thoughtOrder,
}: AdjacentThoughtProps) => {
  const style = css([
    TEXT_STYLE.body[1],
    roundedBorder,
    {
      color: hslFromColorThemeKey(ColorThemeKey.Highlight),
      padding: '8px 16px',
      display: 'flex',
      alignItems: 'center',
      textAlign: 'center',
      marginLeft: thoughtOrder === ThoughtOrder.Next ? 'auto' : '',
      marginRight:
        thoughtOrder === ThoughtOrder.Previous ? 'auto' : '',
    },
  ])

  const text =
    thoughtOrder === ThoughtOrder.Next ? `${title} >>` : `<< ${title}`

  return (
    <DwLink to={slug} style={style}>
      {text}
    </DwLink>
  )
}

export { ThoughtOrder, AdjacentThought }
