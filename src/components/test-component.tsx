import React from 'react'
import { css } from '@emotion/core'
import {
  ColorThemeKey,
  hslaFromColorThemeKey,
} from '@styles/color-theme'

const TestComponent = () => {
  const style = css({
    height: '200px',
    width: '200px',
    background: hslaFromColorThemeKey(
      ColorThemeKey.PrimaryBackground
    ),
  })

  return <div css={style}>yao</div>
}

export { TestComponent }
