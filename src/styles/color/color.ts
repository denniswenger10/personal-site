import { hsl } from './hsl'

const COLORS = {
  bismark: hsl(198, 26),
  mint: hsl(162, 63),
  gray: hsl(330, 0),
  cerulean: hsl(227, 63),
  lightning: hsl(43, 95),
  persian: hsl(3, 61),
  gold: hsl(43, 55),
  orchid: hsl(272, 52),
  pearly: hsl(320, 27),
  chateau: hsl(143, 50),
}

export { COLORS }
