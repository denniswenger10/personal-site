import { curry } from 'ramda'

const hsl = curry(
  (hue: number, saturation: number, lightness: number) =>
    `${hue}, ${saturation}%, ${lightness}%`
)

export { hsl }
