enum ColorThemeKey {
  /**
   * The main background of the website
   */
  PrimaryBackground = '--primary_background',
  /**
   * Primarily the text appearing on the primary background
   */
  PrimaryForeground = '--primary_foreground',
  /**
   * Secondary background elements, like the navbar
   */
  SecondaryBackground = '--secondary_background',
  /**
   * Primarily text appearing on secondary background
   */
  SecondaryForeground = '--secondary_foreground',
  /**
   * Same as foreground but lower value
   */
  ForegroundContrastHigh = '--foreground_contrast-high',
  /**
   * Same as foreground but lower value
   */
  ForegroundContrastMedium = '--foreground_contrast-medium',
  /**
   * Same as foreground but lower value
   */
  ForegroundContrastLow = '--foreground_contrast-low',
  /**
   * On elements that should stand out
   */
  Accent = '--accent',
  /**
   * Primarily for links
   */
  Highlight = '--highlight',
  /**
   * Links when being hovered
   */
  HighlightHover = '--highlight_hover',
  /**
   * For things that should always be black, you probably want this to be full black
   */
  AlwaysBlack = '--always_black',
  /**
   * For things that should always be white, you probably want this to be full white
   */
  AlwaysWhite = '--always_white',
  /**
   * For lines between items, borders, etc.
   */
  Divider = '--divider', // Lines between items, borders, etc.
}

export { ColorThemeKey }
