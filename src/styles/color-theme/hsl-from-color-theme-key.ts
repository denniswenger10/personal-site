import { ColorThemeKey } from './color-theme-key'

/**
 * Will simply turn the `colorThemeKey` into a css consumable string
 *
 * @param  {ColorThemeKey} colorThemeKey
 */
const hslFromColorThemeKey = (colorThemeKey: ColorThemeKey) =>
  `hsl(var(${colorThemeKey}))`

export { hslFromColorThemeKey }
