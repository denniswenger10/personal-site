import { ColorThemeKey } from '../color-theme-key'
import { COLORS } from '@src/styles/color'

const colorThemeLight: [ColorThemeKey, string][] = [
  [ColorThemeKey.PrimaryBackground, COLORS.gray(100)],
  [ColorThemeKey.PrimaryForeground, COLORS.gray(20)],
  [ColorThemeKey.SecondaryBackground, COLORS.bismark(20)],
  [ColorThemeKey.SecondaryForeground, COLORS.gray(100)],
  [ColorThemeKey.ForegroundContrastHigh, COLORS.gray(30)],
  [ColorThemeKey.ForegroundContrastMedium, COLORS.gray(50)],
  [ColorThemeKey.ForegroundContrastLow, COLORS.gray(60)],
  [ColorThemeKey.Accent, COLORS.mint(50)],
  [ColorThemeKey.Highlight, COLORS.mint(45)],
  [ColorThemeKey.HighlightHover, COLORS.mint(50)],
  [ColorThemeKey.AlwaysBlack, COLORS.gray(0)],
  [ColorThemeKey.AlwaysWhite, COLORS.gray(100)],
  [ColorThemeKey.Divider, COLORS.bismark(60)],
]

export { colorThemeLight }
