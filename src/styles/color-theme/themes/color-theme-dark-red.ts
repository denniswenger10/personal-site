import { ColorThemeKey } from '../color-theme-key'
import { COLORS } from '@src/styles/color'

const colorThemeDarkRed: [ColorThemeKey, string][] = [
  [ColorThemeKey.PrimaryBackground, COLORS.gray(10)],
  [ColorThemeKey.PrimaryForeground, COLORS.gray(95)],
  [ColorThemeKey.SecondaryBackground, COLORS.persian(50)],
  [ColorThemeKey.SecondaryForeground, COLORS.gray(95)],
  [ColorThemeKey.ForegroundContrastHigh, COLORS.gray(80)],
  [ColorThemeKey.ForegroundContrastMedium, COLORS.gray(50)],
  [ColorThemeKey.ForegroundContrastLow, COLORS.gray(30)],
  [ColorThemeKey.Accent, COLORS.persian(55)],
  [ColorThemeKey.Highlight, COLORS.persian(60)],
  [ColorThemeKey.HighlightHover, COLORS.persian(50)],
  [ColorThemeKey.AlwaysBlack, COLORS.gray(0)],
  [ColorThemeKey.AlwaysWhite, COLORS.gray(100)],
  [ColorThemeKey.Divider, COLORS.gray(36)],
]

export { colorThemeDarkRed }
