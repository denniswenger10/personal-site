import { ColorThemeKey } from '../color-theme-key'
import { COLORS } from '@src/styles/color'

const colorThemeStandard: [ColorThemeKey, string][] = [
  [ColorThemeKey.PrimaryBackground, COLORS.bismark(10)],
  [ColorThemeKey.PrimaryForeground, COLORS.gray(100)],
  [ColorThemeKey.SecondaryBackground, COLORS.bismark(5)],
  [ColorThemeKey.SecondaryForeground, COLORS.gray(100)],
  [ColorThemeKey.ForegroundContrastHigh, COLORS.gray(80)],
  [ColorThemeKey.ForegroundContrastMedium, COLORS.gray(50)],
  [ColorThemeKey.ForegroundContrastLow, COLORS.gray(30)],
  [ColorThemeKey.Accent, COLORS.mint(70)],
  [ColorThemeKey.Highlight, COLORS.mint(60)],
  [ColorThemeKey.HighlightHover, COLORS.mint(40)],
  [ColorThemeKey.AlwaysBlack, COLORS.gray(0)],
  [ColorThemeKey.AlwaysWhite, COLORS.gray(100)],
  [ColorThemeKey.Divider, COLORS.bismark(36)],
]

export { colorThemeStandard }
