import { ColorThemeKey } from './color-theme-key'

/**
 * Will simply turn the `colorThemeKey` into a css consumable string
 *
 * @param  {ColorThemeKey} colorThemeKey
 * @param  {number=1} alpha - A number between 0 and 1
 */
const hslaFromColorThemeKey = (
  colorThemeKey: ColorThemeKey,
  alpha: number = 1
) => `hsla(var(${colorThemeKey}), ${alpha})`

export { hslaFromColorThemeKey }
