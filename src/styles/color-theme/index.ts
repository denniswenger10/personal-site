import { ColorThemeKey } from './color-theme-key'

type ColorTheme = [ColorThemeKey, string][]

export * from './hsl-from-color-theme-key'
export * from './hsla-from-color-theme-key'
export { ColorTheme, ColorThemeKey }
