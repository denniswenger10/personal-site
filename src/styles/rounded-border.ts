import { css } from '@emotion/core'
import {
  ColorThemeKey,
  hslFromColorThemeKey,
} from '@styles/color-theme'

const roundedBorder = css({
  borderRadius: '5px',
  border: `1px solid ${hslFromColorThemeKey(ColorThemeKey.Divider)}`,
})

export { roundedBorder }
