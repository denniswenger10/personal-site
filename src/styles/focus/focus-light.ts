import { css } from '@emotion/core'
import { ColorThemeKey, hslaFromColorThemeKey } from '../color-theme'

const getColor = (alpha: number) =>
  hslaFromColorThemeKey(ColorThemeKey.Highlight, alpha)

const focusLight = css({
  position: 'relative',
  '&::before,&::after': {
    content: '""',
    position: 'absolute',
    opacity: 0,
    transform: 'scaleY(0)',
    transformOrigin: 'bottom',
  },
  '&::after': {
    transition: 'all 0.35s ease',
    bottom: '-2px',
    left: '-2px',
    filter: 'blur(12px)',
    height: '100%',
    width: 'calc(100% + 4px)',
    background: `linear-gradient(0deg, ${getColor(
      0.2
    )} 0%, ${getColor(0.1)} 30%, ${getColor(0)} 100%)`,
  },
  '&::before': {
    transition: 'all 0.25s ease',
    bottom: 0,
    left: '-1px',
    width: 'calc(100% + 2px)',
    height: '4px',
    filter: 'blur(2px)',
    borderRadius: '9999px',
    background: getColor(0.6),
  },
  '&:hover,&:active': {
    '&::before,&::after': {
      opacity: 1,
      transform: 'scale(1)',
    },
  },
})

export { focusLight }
