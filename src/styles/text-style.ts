import { css } from '@emotion/core'

const TEXT_STYLE = {
  heading: {
    '1': css({
      fontFamily: '"Londrina Solid", sans-serif',
      fontStyle: 'normal',
      fontWeight: 300,
      fontSize: '48px',
      lineHeight: '54px',
      letterSpacing: '0em',
    }),
    '2': css({
      fontFamily: '"Londrina Solid", sans-serif',
      fontStyle: 'normal',
      fontWeight: 300,
      fontSize: '38px',
      lineHeight: '44px',
      letterSpacing: '0em',
    }),
    '3': css({
      fontFamily: '"Londrina Solid", sans-serif',
      fontStyle: 'normal',
      fontWeight: 300,
      fontSize: '28px',
      lineHeight: '33px',
      letterSpacing: '0em',
    }),
    '4': css({
      fontFamily: '"Londrina Solid", sans-serif',
      fontStyle: 'normal',
      fontWeight: 300,
      fontSize: '20px',
      lineHeight: '24px',
      letterSpacing: '0em',
    }),
    '5': css({
      fontFamily: '"Baloo Chettan 2", sans-serif',
      fontStyle: 'normal',
      fontWeight: 300,
      fontSize: '16px',
      lineHeight: '23px',
      letterSpacing: '0.03em',
    }),
  },
  preHeading: {
    '1': css({
      fontFamily: '"Baloo Chettan 2", sans-serif',
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '20px',
      lineHeight: '24px',
      letterSpacing: '0.03em',
      textTransform: 'uppercase',
    }),
    '2': css({
      fontFamily: '"Baloo Chettan 2", sans-serif',
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '16px',
      lineHeight: '22px',
      letterSpacing: '0.03em',
      textTransform: 'uppercase',
    }),
    '3': css({
      fontFamily: '"Baloo Chettan 2", sans-serif',
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: '12px',
      lineHeight: '17px',
      letterSpacing: '0.03em',
      textTransform: 'uppercase',
    }),
  },
  subtitle: {
    '1': css({
      fontFamily: '"Baloo Chettan 2", sans-serif',
      fontStyle: 'italic',
      fontWeight: 300,
      fontSize: '16px',
      lineHeight: '22px',
      letterSpacing: '0.06em',
    }),
    '2': css({
      fontFamily: '"Baloo Chettan 2", sans-serif',
      fontStyle: 'italic',
      fontWeight: 300,
      fontSize: '12px',
      lineHeight: '17px',
      letterSpacing: '0.03em',
    }),
  },
  body: {
    '1': css({
      fontFamily: '"Baloo Chettan 2", sans-serif',
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '16px',
      lineHeight: '24px',
      letterSpacing: '0em',
    }),
    '2': css({
      fontFamily: '"Baloo Chettan 2", sans-serif',
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '12px',
      lineHeight: '17px',
      letterSpacing: '0em',
    }),
  },
  button: {
    '1': css({
      fontFamily: '"Baloo Chettan 2", sans-serif',
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '12px',
      lineHeight: '17px',
      letterSpacing: '0.065em',
      textTransform: 'uppercase',
    }),
  },
  caption: {
    // Not approved yet
    '1': css({
      fontFamily: '"Baloo Chettan 2", sans-serif',
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '11px',
      lineHeight: '16px',
      letterSpacing: '0.5px',
    }),
  },
  overline: {
    // Not approved yet
    '1': css({
      fontFamily: '"Baloo Chettan 2", sans-serif',
      fontStyle: 'normal',
      fontWeight: 500,
      fontSize: '10px',
      lineHeight: '16px',
      letterSpacing: '1.5px',
    }),
  },
  code: {
    '1': css({
      fontFamily: '"Fira Code", monospace',
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '16px',
      lineHeight: '24px',
      letterSpacing: '0em',
    }),
    '2': css({
      fontFamily: '"Fira Code", monospace',
      fontStyle: 'normal',
      fontWeight: 400,
      fontSize: '12px',
      lineHeight: '17px',
      letterSpacing: '0.02em',
    }),
  },
}

export { TEXT_STYLE }
