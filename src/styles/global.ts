import {
  ColorThemeKey,
  hslaFromColorThemeKey,
  hslFromColorThemeKey,
} from '@styles/color-theme'
import { css } from '@emotion/core'
import { resetGlobal } from './reset'

const GLOBAL = css([
  resetGlobal,
  {
    body: {
      fontFamily: 'Baloo Chettan 2',
      background: hslFromColorThemeKey(
        ColorThemeKey.PrimaryBackground
      ),
      fontcolor: hslFromColorThemeKey(
        ColorThemeKey.PrimaryForeground
      ),
      /* 
        TODO:
        Just to make sure content starts below navbar. 
        Got to find a better way to do this
      */
      paddingTop: '64px',
    },

    '::-webkit-scrollbar-thumb': {
      backgroundColor: hslaFromColorThemeKey(
        ColorThemeKey.ForegroundContrastLow,
        0.6
      ),
      width: '8px',
      height: '8px',
      borderRadius: '1000px',
    },
    '::-webkit-scrollbar-track': {
      borderRadius: '9999px',
      boxShadow: `inset 0 0 8px ${hslaFromColorThemeKey(
        ColorThemeKey.AlwaysBlack,
        0.3
      )}`,
    },
    '::-webkit-scrollbar': {
      width: '8px',
      height: '8px',
    },
  },
])

export { GLOBAL }
