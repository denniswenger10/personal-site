import { useContext } from 'react'
import { ColorThemeContext } from '@context'

const useColorContext = () => useContext(ColorThemeContext)

export { useColorContext }
