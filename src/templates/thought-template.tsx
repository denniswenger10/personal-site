import React from 'react'
import { graphql, PageProps } from 'gatsby'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import { css } from '@emotion/core'
import { LayoutMdx } from '@components/layout'
import { TEXT_STYLE } from '@styles'
import {
  ColorThemeKey,
  hslFromColorThemeKey,
} from '@styles/color-theme'
import { Hr, H1 } from '@components/base'
import {
  AdjacentThought,
  ThoughtOrder,
  AdjacentThoughtSection,
} from '@components/page-components/thought-template'
import { filterMap } from '@common'

type AdjacentThoughtData = {
  slug: string
  title: string
}

type ThoughtTemplateProps = PageProps & {
  data: {
    mdx: {
      body: string
      frontmatter: {
        title: string
        description: string
        lastUpdated: string
        creationDate: string
      }
    }
  }
  pageContext: {
    previous: AdjacentThoughtData
    next: AdjacentThoughtData
  }
}

const ThoughtTemplate = ({
  data,
  pageContext,
}: ThoughtTemplateProps) => {
  const { previous, next } = pageContext
  const { frontmatter, body } = data.mdx
  const { lastUpdated, creationDate } = frontmatter

  const subtitle = css([
    TEXT_STYLE.subtitle[1],
    {
      color: hslFromColorThemeKey(
        ColorThemeKey.ForegroundContrastHigh
      ),
    },
  ])

  const subtitleText =
    lastUpdated === creationDate
      ? `Published ${creationDate}`
      : `Last updated ${lastUpdated}`

  const adjacentThoughts = filterMap(
    (thought, i) => {
      if (thought) {
        return {
          ...thought,
          thoughtOrder:
            i === 0 ? ThoughtOrder.Previous : ThoughtOrder.Next,
        }
      }
    },
    [previous, next]
  )

  return (
    <LayoutMdx>
      <article>
        <H1>{frontmatter.title}</H1>
        <Hr />
        <p css={subtitle}>{subtitleText}</p>
        <MDXRenderer>{body}</MDXRenderer>
        <AdjacentThoughtSection>
          {adjacentThoughts.map(({ slug, thoughtOrder, title }) => (
            <AdjacentThought
              key={slug}
              slug={slug}
              thoughtOrder={thoughtOrder}
              title={title}
            />
          ))}
        </AdjacentThoughtSection>
      </article>
    </LayoutMdx>
  )
}

export default ThoughtTemplate

export const pageQuery = graphql`
  query($slug: String!) {
    mdx(fields: { slug: { eq: $slug } }) {
      body
      frontmatter {
        title
        description
        lastUpdated(formatString: "D MMM, YYYY")
        creationDate(formatString: "D MMM, YYYY")
      }
    }
  }
`
