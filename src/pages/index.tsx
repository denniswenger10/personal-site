import React from 'react'
import { PageProps, graphql } from 'gatsby'
import { css } from '@emotion/core'
import { StandardLayout } from '@components/layout'
import {
  Landing,
  ThoughtsSection,
} from '@src/components/page-components/home'
import {} from 'gatsby-transformer-remark'

const Home = ({ data }: PageProps) => {
  const style = css({
    display: 'grid',
    rowGap: '48px',
  })

  return (
    <StandardLayout>
      <div css={style}>
        <Landing />
        <ThoughtsSection />
      </div>
    </StandardLayout>
  )
}

export default Home

// export const pageQuery = graphql`
//   query {
//     allMarkdownRemark(
//       sort: { fields: frontmatter___date, order: DESC }
//     ) {
//       nodes {
//         fields {
//           minutesToRead
//           slug
//         }
//         frontmatter {
//           title
//           date(formatString: "D MMM, YYYY")
//         }
//       }
//     }
//   }
// `
