import React from 'react'
import { PageProps, graphql } from 'gatsby'
import { css } from '@emotion/core'
import { StandardLayout } from '@components/layout'

const Thoughts = ({ data }: PageProps) => {
  const style = css({
    display: 'grid',
    rowGap: '48px',
  })

  return (
    <StandardLayout>
      <div css={style}>Thoughts page</div>
    </StandardLayout>
  )
}

export default Thoughts

// export const pageQuery = graphql`
//   query {
//     allMarkdownRemark(
//       sort: { fields: frontmatter___date, order: DESC }
//     ) {
//       nodes {
//         fields {
//           minutesToRead
//           slug
//         }
//         frontmatter {
//           title
//           date(formatString: "D MMM, YYYY")
//         }
//       }
//     }
//   }
// `
