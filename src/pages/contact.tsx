import React from 'react'
import { PageProps, graphql } from 'gatsby'
import { css } from '@emotion/core'
import { StandardLayout } from '@components/layout'

const Contact = ({ data }: PageProps) => {
  const style = css({
    display: 'grid',
    rowGap: '48px',
  })

  return (
    <StandardLayout>
      <div css={style}>Contact page</div>
    </StandardLayout>
  )
}

export default Contact

// export const pageQuery = graphql`
//   query {
//     allMarkdownRemark(
//       sort: { fields: frontmatter___date, order: DESC }
//     ) {
//       nodes {
//         fields {
//           minutesToRead
//           slug
//         }
//         frontmatter {
//           title
//           date(formatString: "D MMM, YYYY")
//         }
//       }
//     }
//   }
// `
