import React from 'react'
import { PageProps, graphql } from 'gatsby'
import { css } from '@emotion/core'
import { StandardLayout } from '@components/layout'

const About = ({ data }: PageProps) => {
  const style = css({
    display: 'grid',
    rowGap: '48px',
  })

  return (
    <StandardLayout>
      <div css={style}>About page</div>
    </StandardLayout>
  )
}

export default About

// export const pageQuery = graphql`
//   query {
//     allMarkdownRemark(
//       sort: { fields: frontmatter___date, order: DESC }
//     ) {
//       nodes {
//         fields {
//           minutesToRead
//           slug
//         }
//         frontmatter {
//           title
//           date(formatString: "D MMM, YYYY")
//         }
//       }
//     }
//   }
// `
