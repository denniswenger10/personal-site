import React from 'react'
import { PageProps } from 'gatsby'

const NotFoundPage = ({}: PageProps) => {
  return <div>Not found</div>
}

export default NotFoundPage
