const path = require('path')
const { createFilePath } = require(`gatsby-source-filesystem`)
const { countWords } = require('./src/gatsby/count-words')
const { minutesToRead } = require('./src/gatsby/minutes-to-read')

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions

  const thoughtTemplate = path.resolve(
    './src/templates/thought-template.tsx'
  )

  const result = await graphql(`
    {
      allMdx(
        sort: { fields: [frontmatter___creationDate], order: DESC }
        filter: { frontmatter: { isPublished: { eq: true } } }
      ) {
        nodes {
          fields {
            slug
          }
          frontmatter {
            title
          }
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panicOnBuild(
      `There was an error loading thoughts`,
      result.errors
    )
    return
  }

  const thoughts = result.data.allMdx.nodes

  thoughts.forEach((thought, index) => {
    const previous =
      index === thoughts.length - 1 ? null : thoughts[index + 1]
    const next = index === 0 ? null : thoughts[index - 1]

    createPage({
      path: thought.fields.slug,
      component: thoughtTemplate,
      context: {
        slug: thought.fields.slug,
        previous: previous
          ? {
              slug: previous.fields.slug,
              title: previous.frontmatter.title,
            }
          : null,
        next: next
          ? {
              slug: next.fields.slug,
              title: next.frontmatter.title,
            }
          : null,
      },
    })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })

    createNodeField({
      name: `slug`,
      node,
      value,
    })

    const wordCount = countWords(node.rawMarkdownBody)

    createNodeField({
      name: `wordCount`,
      node,
      value: wordCount,
    })

    createNodeField({
      name: `minutesToRead`,
      node,
      value: minutesToRead(200)(wordCount),
    })
  }

  if (node.internal.type === 'Mdx') {
    const value = createFilePath({ node, getNode })

    createNodeField({
      name: 'slug',
      node,
      value: '/thoughts' + value,
    })

    const wordCount = countWords(node.rawBody)

    createNodeField({
      name: `wordCount`,
      node,
      value: wordCount,
    })

    createNodeField({
      name: `minutesToRead`,
      node,
      value: minutesToRead(200)(wordCount),
    })
  }
}

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions

  // Explicitly define the Markdown frontmatter
  // This way the "MarkdownRemark" queries will return `null` even when no
  // thoughts are stored inside "content/thoughts" instead of returning an error
  createTypes(`
    type Mdx implements Node {
      frontmatter: Frontmatter
      fields: Fields
    }
    type MarkdownRemark implements Node {
      frontmatter: Frontmatter
      fields: Fields
    }
    type Frontmatter {
      title: String
      description: String
      creationDate: Date @dateformat
      isPublished: Boolean
      lastUpdated: Date @dateformat
    }
    type Fields {
      slug: String
      wordCount: Int
      minutesToRead: Int
    }
  `)
}
